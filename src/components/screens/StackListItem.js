import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';
import ListItemScreen from './ListItemScreen';
import ListItemDetailsScreen from './ListItemDetailsScreen';

const StackListItem = createStackNavigator({
  Main: {
    screen: ListItemScreen,
    navigationOptions: { header: null }
  },
  Detail: {
    screen: ListItemDetailsScreen,
    navigationOptions: {
      title: 'Details about person'
    }
  }
});

StackListItem.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

export default StackListItem;