import React, { Component } from "react";
import { View, Text, Image, StyleSheet } from "react-native";

class ListItemDetailsScreen extends Component {

  render() {
    var {params} = this.props.navigation.state;
    // console.log('navigationParams: ', params)
    // console.log(`this.props.navigation: ${JSON.stringify(this.props.navigation)}`)

    return (
      <View style={styles.container}>        
       <View style={styles.detailsRow}>
          <Image style={styles.img} source={{uri: params.dataSource[0].picture}}></Image>
          <View style={styles.dataCol}>
            <Text>Name: {params.dataSource[0].name.first}{' '}{params.dataSource[0].name.last}</Text>
            <Text>Company: {params.dataSource[0].company}</Text>
          </View>
       </View>
        <Text style={styles.about}>{params.dataSource[0].about}</Text>
      </View>
    );
  }
} 
export default ListItemDetailsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 5,
  },
  title: {
    alignSelf: 'flex-start',
  },
  detailsRow: {
    flexDirection: 'row',
    backgroundColor: 'white'
  },
  img: {
    width: 100,
    height: 100,
  },
  dataCol: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  about: {
    margin: 10,
  }
})
