import React, { Component } from "react";
import { View, Text, Image, StyleSheet, FlatList, TouchableOpacity } from "react-native";

class ListItemScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],   
    };
  }

  renderItem = ({ item }) => {
    var tempItem = false;
    if ( item.index % 2 !== 0 ) {
      tempItem = true;
    } else {
      tempItem = false;
    }
    // var {navigate} = this.props.navigation;
      return (        
        <TouchableOpacity 
          onPress={ () => {            
            this.props.navigation.navigate('Detail', { dataSource: [item]})
          }}
            style={[styles.item, tempItem ? styles.itemRight : styles.itemLeft]}>
          <Image 
            style={styles.img}
            source={{ uri: item.picture }} />
          <View style={styles.textContainer}>
            <Text>{item.name.first}{' '}{item.name.last}</Text>
            <Text>{item.company}</Text>
          </View>
        </TouchableOpacity>
      );    
  };

  componentDidMount() {
    const url = "https://next.json-generator.com/api/json/get/NyJDf_VKB";

    fetch(url)
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          dataSource: responseJson.item_array
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.dataSource}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}
export default ListItemScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: -5
  },
  item: {
    flex: 1,
    marginBottom: 5,
    alignItems: 'center',
  },
  itemLeft: {
    flexDirection: 'row',
    backgroundColor: '#f5f5f5'
  },
  itemRight: {
    flexDirection: 'row-reverse',
    backgroundColor: '#f0f8ff',
  },
  img: {
    width: 100,
    height: 100,
    marginHorizontal: 5,
  }
});
