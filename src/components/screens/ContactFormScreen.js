import React, { Component } from "react";
import { View, Text, StyleSheet, TextInput, TouchableOpacity, Alert } from "react-native";

class ContactFormScreen extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      nameValidate: true,
      number: '',
      numberValidate: true,
      message: '',
    }
  }

  updateValue(text, field) {
    alph=/^[a-zA-Z]+$/
    numeric=/^\d{11}$/g

    if (field == 'name') {
      if (alph.test(text))
      {
        this.setState({
          nameValidate: true,
        })
      } 
      else {
        this.setState({
          nameValidate: false,
        })
      }
    }
    else if (field == 'number') {
      if (numeric.test(text))
      {
        this.setState({
          numberValidate: true,
        })
      } 
      else {
        this.setState({
          numberValidate: false,
        })
      }
    }

    this.setState({[field]:text})
  }

  checkIfNotEmpty(){
    const { name } = this.state;
    const { number } = this.state;
    const { message } = this.state;
    if ( name == '' || number == '' || message == '' ) {
      Alert.alert("All fields must be filled");
    }
    else {
      this.submit();
    }
  }
  submit() {
    let collection={};
    collection.name=this.state.name,
    collection.number=this.state.number,
    collection.message=this.state.message

    // console.warn(collection);
    Alert.alert("Thank you for your message!");

    /* Uncomment for data POST and set url */
    // var url = 'https://someweb.com/message/';

    // fetch(url, {
    //   method: 'POST',
    //   body: JSON.stringify(collection),
    //   headers:{
    //     'Content-Type': 'application/json'
    //   }
    // })
    // .then(res => res.json())
    // .then(response => console.log('Success:', JSON.stringify(response)))
    // .catch(error => console.error('Error:', error));

    this.setState( 
      {
        name: '',
        number: '',
        message: '',
      });
    this.textInputName.clear();
    this.textInputNumber.clear();
    this.textInputMessage.clear();
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.heading}>Lets Contact</Text>
        <TextInput 
          placeholder="Your Name"
          style={[styles.input, !this.state.nameValidate ? styles.error : null]}
          onChangeText={(text) => this.updateValue(text, 'name')}
          ref={textInputName => { this.textInputName = textInputName }}
        />
        <TextInput 
          placeholder="Your Phone Number starting with 370"
          style={[styles.input, !this.state.numberValidate ? styles.error : null]}
          onChangeText={(text) => this.updateValue(text, 'number')}
          ref={textInputNumber => { this.textInputNumber = textInputNumber }}
        />
        <TextInput 
          placeholder="Your Message"
          style={styles.input}
          onChangeText={(text) => this.updateValue(text, 'message')}
          ref={textInputMessage => { this.textInputMessage = textInputMessage }}
        />
        <TouchableOpacity 
          onPress={() => this.checkIfNotEmpty()}
          style={styles.btn}>
            <Text style={styles.btnText}>Submit</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
export default ContactFormScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    margin: 5,
  },
  heading: {
    fontSize: 20,
    fontWeight: 'bold', 
    marginBottom: 20,
    textAlign: 'center', 
  },
  input: {
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
  },
  btn: {
    backgroundColor: 'skyblue',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    fontSize: 16,
  },
  error: {
    borderWidth: 3,
    borderColor: 'red'
  }

});