import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import MapView from 'react-native-maps';
import Marker from 'react-native-maps';

class MapScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Map is here</Text>
        <MapView
          style={styles.map}
          region={{
            latitude: 55.688396,
            longitude: 21.144621,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
          }}
          >
           <MapView.Marker
            coordinate={{latitude: 55.688396,
            longitude: 21.144621}}
            title={"title"}
            description={"description"}
            />

          </MapView>
      </View>
    );
  }
}
export default MapScreen;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
 });