/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { createBottomTabNavigator } from 'react-navigation';
import MapScreen from './src/components/screens/MapScreen';
import ContactFormScreen from './src/components/screens/ContactFormScreen';
import StackListItem from './src/components/screens/StackListItem';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class App extends React.Component {
  render() {
    return (
        <AppBottomTabNavigator />
    );
  }
}

const AppBottomTabNavigator = createBottomTabNavigator({
  Map: {
    screen: MapScreen,
    navigationOptions: {
      tabBarLabel: 'Map',
      tabBarIcon: ({ tintColor }) => (
        <Icon name="map" color={tintColor} size={18} />
      )
    }
  },
  ContactForm: {
    screen: ContactFormScreen,
    navigationOptions: {
      tabBarLabel: 'Contact Form',
      tabBarIcon: ({ tintColor }) => (
        <Icon name="wpforms" color={tintColor} size={18} />
      )
    }
  },
  ListItem: {
    screen: StackListItem,
    navigationOptions: ({ navigation }) => ({
      tabBarLabel: 'List Item',
      tabBarIcon: ({ tintColor }) => (
        <Icon name="list" color={tintColor} size={18} />
      )
    })
  },
},
{
  tabBarOptions: {
  activeTintColor: 'tomato',
  inactiveTintColor: 'gray',
  labelStyle: {
  fontSize: 14,
  }
},
} );

